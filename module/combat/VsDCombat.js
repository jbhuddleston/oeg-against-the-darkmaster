import { system } from "../config.js";

export class VsDCombat extends Combat {
  constructor(data, context) {
    super(data, context);
    this.actions = system.combat || [];
  }

  async nextTurn() {
    if (this.turn != 0) {
      await this.update({ turn: 0 });
    }
    if (this.combatant.initiative == null) return;
    await this.combatant.actor.setFlag("vsd", "action", "Completed");
    await this.combatant.update({
      ["flags.vsd.action"]: this.actions[0].action,
      initiative: null
    });
    if (this.turns[0].initiative == null) this.nextRound();
  }

  async nextRound() {
    if (!this.isOwner) {
      ui.notifications.warn("Players may not advance the round, please tell your GM to do so.");
      return;
    };
    await this.resetAll();
    this.combatants.forEach(c => {
      c.actor.resetModVars(true);
      c.actor.applyBleeding();
      c.update({
        ['flags.vsd.ready']: false,
        ["flags.vsd.action"]: this.actions[0].action,
        initiative: null
      });
    });
    return super.nextRound();
  }
}

export class VsDCombatTracker extends CombatTracker {
  get template() {
    return "systems/vsd/templates/combat/vsdcombat-tracker.hbs";
  }

  async getData(options) {
    const data = await super.getData(options);
    if (!data.hasCombat) {
      return data;
    }
    for (let i = 0; i < data.turns.length; i++) {
      const turn = data.turns[i];
      const combatant = this.viewed.combatants.get(turn.id);
      turn.action = combatant.getFlag("vsd", "action");
      turn.ready = combatant.getFlag("vsd", "ready");
      turn.active = i == 0;
      turn.css = turn.defeated ? "defeated" : turn.active ? "active" : "";
    }
    return data;
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find(".declaredaction").change(this._onActionsChanged.bind(this));
    html.find(".interrupt").click(this._onInterrupt.bind(this));
    html.find(".name").click(this._onToggleReady.bind(this));
  }

  async _onToggleReady(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;
    await c.setFlag("vsd", "ready", !c.getFlag("vsd", "ready"));
  }

  async _onActionsChanged(event) {
    const option = event.currentTarget.selectedOptions[0];
    const li = event.currentTarget.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;

    await c.update({
      ["flags.vsd.action"]: option.text,
      initiative: Number(option.value)
    });
    await c.actor.setFlag("vsd", "action", option.text);
  }

  async _onInterrupt(event) {
    const btn = event.currentTarget;
    const li = btn.closest(".combatant");
    const c = this.viewed.combatants.get(li.dataset.combatantId);
    if (!c.isOwner) return;
    return c.update({
      initiative: this.viewed.turns[0].initiative + 1
    });
  }
}

export class VsDCombatantConfig extends CombatantConfig {
  get template() {
    return "systems/vsd/templates/combat/vsdcombatant-config.hbs";
  }
}

export class VsDCombatant extends Combatant {
  constructor(data, context) {
    super(data, context);
  }

  _onCreate(data, options, userId) {
    super._onCreate(data, options, userId);
    if (this.isOwner) {
      this.setFlag("vsd", "action", "Select");
      data.initiative = null;
    }
  }
}
