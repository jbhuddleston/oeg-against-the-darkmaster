/**
 * Roll a SciFantaPunk dice pool roll
 * Author: Legendmaster
 * Based on macro by: Caerandir
 */

new Dialog({
  title: `Roll dice pool of d10`,
  content: `
      <form>
          <div style="display: flex; width: 100%; margin-bottom: 10px">
              <label for="numberOfDice" style="white-space: nowrap; margin-right: 10px; padding-top:4px">Number of d6:</label>
              <input type="number" id="numberOfDice" name="numberOfDice" min="1" max="20" autofocus />
          </div>
          <div style="display: flex; width: 100%; margin-bottom: 10px">
              <label for="difficulty" style="white-space: nowrap; margin-right: 10px; padding-top:4px">Difficulty:</label>
          </div>
          <div style="display: flex; width: 100%; margin-bottom: 10px">
              <input type="radio" id="d2" name="difficulty" value="2" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">2</label>
              <input type="radio" id="d3" name="difficulty" value="3" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">3</label>
              <input type="radio" id="d4" name="difficulty" value="4" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">4</label>
              <input type="radio" id="d5" name="difficulty" value="5" checked="checked" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">5</label>
              <input type="radio" id="d6" name="difficulty" value="6" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">6</label>
              <input type="radio" id="d7" name="difficulty" value="7" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">7</label>
              <input type="radio" id="d8" name="difficulty" value="8" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">8</label>
              <input type="radio" id="d9" name="difficulty" value="9" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">9</label>
              <input type="radio" id="d10" name="difficulty" value="10" />
              <label for="normal" style="white-space: nowrap; margin-left: 4px; margin-right: 7px; padding-top:4px">10</label>
          </div>            
      </form>
  `,
  buttons: {
      yes: {
          icon: "<i class='fas fa-check'></i>",
          label: `Roll!`,
          callback: async (html) => {
              let numberOfDice = html.find('#numberOfDice').val();
              let difficulty = html.find('[name="difficulty"]:checked').val();
              if (!numberOfDice) {
                  return ui.notifications.info("Please enter number of d10.");
              }
              let rollCommand = numberOfDice;
              rollCommand = rollCommand.concat('d10');
              console.log("rollCommand as number of dice " + rollCommand);
              rollCommand = rollCommand.concat('cs>=', difficulty);
              //rollCommand = rollCommand.concat('cs>=', difficulty, 'df1');
              console.log("rollCommand as full roll string " + rollCommand);
              const roll = await new Roll(rollCommand).evaluate();
              console.log("roll result " + roll.result);
              var rollResult = roll.result;
              console.log("rollResult variable " + rollResult);
              console.log("roll total " + roll.total);
              //roll.roll();
              roll.toMessage();
              //const inlineCheck = TextEditor.enrichHTML(rollResult);
              //ChatMessage.create({content: inlineCheck});
          }
      },
      no: {
          icon: "<i class='fas fa-times'></i>",
          label: `Cancel`
      },
  },
  default: "yes",
  render: html => html.find('#numberOfDice').focus()
}).render(true);