// Open a dialog for quickly changing token vision parameters of the controlled tokens.

if (canvas.tokens.controlled.length === 0)
    return ui.notifications.error("Please select a token first");

let applyChanges = false;
new Dialog({
    title: `Lighting Configuration`,
    content: `
<form>
    <div class="form-group">
        <label>Light Conditions:</label>
        <select id="light-conditions" name="light-conditions">
            <option value="nochange">No Change</option>
            <option value="dark">Darkness</option>
            <option value="star">Starlight</option>
            <option value="moon">Moonlight</option>
            <option value="dim3">Dim Light (3 m)</option>
            <option value="dim6">Dim Light (6 m)</option>
            <option value="dim9">Dim Light (9 m)</option>
            <option value="dim12">Dim Light (12 m)</option>
            <option value="bright6">Bright Light (6 m)</option>
            <option value="bright12">Bright Light (12 m)</option>
            <option value="bright18">Bright Light (18 m)</option>
            <option value="bright24">Bright Light (24 m)</option>
        </select>
    </div>
    <div class="form-group">
        <label>Light Source:</label>
        <select id="light-source" name="light-source">
            <option value="nochange">No Change</option>
            <option value="none">None</option>
            <option value="hooded-dim">Lantern (Hooded - Dim)</option>
            <option value="candle">Candle</option>
            <option value="light">Light (Cantrip)</option>
            <option value="torch">Torch</option>
            <option value="lamp">Lamp</option>
            <option value="hooded-bright">Lantern (Hooded - Bright)</option>
            <option value="bullseye">Lantern (Bullseye)</option>
        </select>
    </div>
</form>
    `,
    buttons: {
        yes: {
            icon: "<i class='fas fa-check'></i>",
            label: `Apply Changes`,
            callback: () => applyChanges = true
        },
        no: {
            icon: "<i class='fas fa-times'></i>",
            label: `Cancel Changes`
        },
    },
    default: "yes",
    close: html => {
        if (applyChanges) {
            for (let token of canvas.tokens.controlled) {
                let visionType = token.actor?.system.dynamic.visionType || "normal";
                let lightConditions = html.find('[name="light-conditions"]')[0].value || "none";
                let lightSource = html.find('[name="light-source"]')[0].value || "none";
                let sightrange = 0;
                let dimLight = 0;
                let brightLight = 0;
                let lightAngle = 360;
                let lockRotation = token.document.lockRotation;
                // Get Vision Type Values
                switch (visionType) {
                    case "normal":
                        switch (lightConditions) {
                            case "dark": {
                                sightrange = 0;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "dim3": {
                                sightrange = 3;
                                break;
                            }
                            case "dim6": {
                                sightrange = 6;
                                break;
                            }
                            case "dim9": {
                                sightrange = 9;
                                break;
                            }
                            case "dim12": {
                                sightrange = 12;
                                break;
                            }
                            case "bright6": {
                                sightrange = 12;
                                break;
                            }
                            case "bright12": {
                                sightrange = 24;
                                break;
                            }
                            case "bright18": {
                                sightrange = 36;
                                break;
                            }
                            case "bright24": {
                                sightrange = 48;
                                break;
                            }
                            default: {
                                sightrange = token.document.sight.range;
                            }
                        }
                        break;
                    case "Star Sight":
                    case "Keen Senses":
                        switch (lightConditions) {
                            case "dark": {
                                sightrange = 0;
                                break;
                            }
                            case "dim3":
                            case "dim6":
                            case "dim9":
                            case "dim12": {
                                sightrange = 30;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "bright6":
                            case "bright12":
                            case "bright18":
                            case "bright24": {
                                sightrange = 240;
                                break;
                            }
                            default: {
                                sightrange = token.document.sight.range;
                            }
                        }
                        break;
                    case "Dark Sight":
                        switch (lightConditions) {
                            case "dark": {
                                sightrange = 3;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "dim3":
                            case "dim6":
                            case "dim9":
                            case "dim12": {
                                sightrange = 30;
                                break;
                            }
                            case "bright6": {
                                sightrange = 24;
                                break;
                            }
                            case "bright12": {
                                sightrange = 48;
                                break;
                            }
                            case "bright18": {
                                sightrange = 72;
                                break;
                            }
                            case "bright24": {
                                sightrange = 96;
                                break;
                            }
                            default: {
                                sightrange = token.document.sight.range;
                            }
                        }
                        break;
                    case "Night Sight":
                        switch (lightConditions) {
                            case "dark": {
                                sightrange = 3;
                                break;
                            }
                            case "dim3":
                            case "dim6":
                            case "dim9":
                            case "dim12": {
                                sightrange = 30;
                                break;
                            }
                            case "star":
                            case "moon":
                            case "bright6":
                            case "bright12":
                            case "bright18":
                            case "bright24": {
                                sightrange = 240;
                                break;
                            }
                            default: {
                                sightrange = token.document.sight.range;
                            }
                        }
                        break;
                    case "Darkvision":
                        sightrange = 240;
                        break;
                }
                // Get Light Source Values
                switch (lightSource) {
                    case "none":
                        dimLight = 0;
                        brightLight = 0;
                        break;
                    case "hooded-dim":
                        dimLight = 1.5;
                        brightLight = 0;
                        break;
                    case "candle":
                        dimLight = 3;
                        brightLight = 1.5;
                        break;
                    case "light":
                        dimLight = 6;
                        brightLight = 3;
                        break;
                    case "torch":
                        dimLight = 12;
                        brightLight = 6;
                        break;
                    case "lamp":
                        dimLight = 13.5;
                        brightLight = 4.5;
                        break;
                    case "hooded-bright":
                        dimLight = 18;
                        brightLight = 9;
                        break;
                    case "bullseye":
                        dimLight = 36;
                        brightLight = 18;
                        lockRotation = false;
                        lightAngle = 52.5;
                        break;
                    case "nochange":
                    default:
                        dimLight = token.document.light.dim;
                        brightLight = token.document.light.bright;
                        lightAngle = token.document.light.angle;
                        lockRotation = token.document.lockRotation;
                }
                // Update Token
                console.debug(`Updating Light settings for ${token.document.name} with ${visionType} vision:\n`, updateData);
                token.updateVisionSource({
                    enabled: true,
                    range: sightrange
                });
                token.updateLightSource({
                    alpha: 0.3,
                    color: "#808040",
                    dim: dimLight,
                    bright: brightLight,
                    angle: lightAngle
                });
                token.updateSource({lockRotation});
            }
        }
    }
}).render(true);